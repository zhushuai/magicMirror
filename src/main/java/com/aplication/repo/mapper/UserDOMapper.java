package com.aplication.repo.mapper;

import com.aplication.domains.UserDO;

import java.util.List;

/**
 * Created by Administrator on 2017/6/17.
 */
public interface UserDOMapper {
    public void userInfoInsert(UserDO userDO);
    public List<UserDO> queryUserInfo(UserDO userDO);
    public void updateUserInfo (UserDO userDO);
}
