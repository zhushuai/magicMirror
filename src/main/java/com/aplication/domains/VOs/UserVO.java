package com.aplication.domains.VOs;

import java.util.Date;

/**
 * Created by Administrator on 2017/6/16.
 */
public class UserVO {
    private long id;
    private Date createDate;
    private Date updateDate;
    private String accountName;
    private String userName;
    private Date birthday;
    private String email;
    private String sex;
    private Boolean isDeleted;
    private String mobilePhone;

    public UserVO(){

    }
    public UserVO(long id, Date createDate, Date updateDate, String accountName, String userName, Date birthday, String email, String sex, Boolean isDeleted, String mobilePhone) {
        this.id = id;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.accountName = accountName;
        this.userName = userName;
        this.birthday = birthday;
        this.email = email;
        this.sex = sex;
        this.isDeleted = isDeleted;
        this.mobilePhone = mobilePhone;
    }

    public long getId() {
        return id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getUserName() {
        return userName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getSex() {
        return sex;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }
}
