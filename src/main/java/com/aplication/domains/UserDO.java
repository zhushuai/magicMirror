package com.aplication.domains;

import java.util.Date;

/**
 * Created by Administrator on 2017/6/16.
 */
public class UserDO {
    private long id;
    private Date createDate;
    private Date updateDate;
    private String accountName;
    private String password;
    private String userName;
    private Date birthday;
    private String email;
    private String sex;
    private Boolean isDeleted;
    private String mobilePhone;

    public UserDO(){

    }

    public UserDO(long id, Date createDate, Date updateDate, String accountName, String password, String userName, Date birthday, String email, String sex, Boolean isDeleted, String mobilePhone) {
        this.id = id;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.accountName = accountName;
        this.password = password;
        this.userName = userName;
        this.birthday = birthday;
        this.email = email;
        this.sex = sex;
        this.isDeleted = isDeleted;
        this.mobilePhone = mobilePhone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }
}
