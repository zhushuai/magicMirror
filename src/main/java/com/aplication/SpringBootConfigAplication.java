package com.aplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Administrator on 2017/6/14.
 */
@SpringBootApplication
public class SpringBootConfigAplication {
public static void main(String[] args){
    SpringApplication.run(SpringBootConfigAplication.class, args);

}
}
