import com.aplication.domains.UserDO;
import com.aplication.repo.mapper.UserDOMapper;
import org.apache.ibatis.session.SqlSession;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/6/17.
 */
public class DOtest {

    public static void main(String[] args){

        UserDO userDO = new UserDO();
        userDO.setId(1);
        userDO.setEmail("1234@qq.com");
       // userDO.setAccountName("zhushuai");
       /* userDO.setBirthday(new Date());
        userDO.setDeleted(false);
        userDO.setCreateDate(new Date());
        userDO.setEmail("454942607@qq.com");
        userDO.setSex("man");
        userDO.setMobilePhone("18934574695");
        userDO.setPassword("111");*/
        SqlSession session = DBTools.getSession();
        UserDOMapper userDOMapper =  session.getMapper(UserDOMapper.class);

        try {
            userDOMapper.updateUserInfo(userDO);
            List<UserDO> list = userDOMapper.queryUserInfo(userDO);
            UserDO userDO1 = list.get(0);
            System.out.println(userDO1.getEmail());
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        }


    }
}
